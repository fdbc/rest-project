var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);


app.get('/clientes',function(req,res) {
  res.send([{
            "CustomerID": "ALFKI",
            "CompanyName": "Alfreds Futterkiste",
            "ContactName": "Maria Anders",
            "ContactTitle": "Sales Representative",
            "Address": "Obere Str. 57",
            "City": "Berlin",
            "Region": null,
            "PostalCode": "12209",
            "Country": "Germany",
            "Phone": "030-0074321",
            "Fax": "030-0076545"
        },
        {
            "CustomerID": "ANATR",
            "CompanyName": "Ana Trujillo Emparedados y helados",
            "ContactName": "Ana Trujillo",
            "ContactTitle": "Owner",
            "Address": "Avda. de la Constitución 2222",
            "City": "México D.F.",
            "Region": null,
            "PostalCode": "05021",
            "Country": "Mexico",
            "Phone": "(5) 555-4729",
            "Fax": "(5) 555-3745"
        },
        {
            "CustomerID": "ANTON",
            "CompanyName": "Antonio Moreno Taquería",
            "ContactName": "Antonio Moreno",
            "ContactTitle": "Owner",
            "Address": "Mataderos  2312",
            "City": "México D.F.",
            "Region": null,
            "PostalCode": "05023",
            "Country": "Mexico",
            "Phone": "(5) 555-3932",
            "Fax": null
        }]);
});

app.get('/clientes/:idcliente',function(req,res) {
  //res.send('Aqui tienes el cliente numero ' + req.params.idcliente);
    res.send({
        "CustomerID": "ANTON",
        "CompanyName": "Antonio Moreno Taquería",
        "ContactName": "Antonio Moreno",
        "ContactTitle": "Owner",
        "Address": "Mataderos  2312",
        "City": "México D.F.",
        "Region": null,
        "PostalCode": "05023",
        "Country": "Mexico",
        "Phone": "(5) 555-3932",
        "Fax": null
    })

});

app.post('/clientes/:idcliente',function(req,res) {
  res.send('cliente dado de alta'  + req.params.idcliente);
  //console.log(req.body);
});

app.post('/clientes/',function(req,res) {
  res.send('error, es necesario especificar un número de cliente');
});
